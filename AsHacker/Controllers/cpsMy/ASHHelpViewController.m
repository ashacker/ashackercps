//
//  ASHHelpViewController.m
//  AsHackerCPS
//
//  Created by 陈涛 on 2018/6/1.
//  Copyright © 2018年 Sinanbell. All rights reserved.
//

#import "ASHHelpViewController.h"

@interface ASHHelpViewController ()
@property(nonatomic,strong)UIWebView *webView;
@end

@implementation ASHHelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     self.navigationController.navigationBar.hidden = NO;
    
    
//    UIWebView *webview = [[UIWebView alloc]init];
//    //    webview.backgroundColor = [UIColor redColor];
//    webview.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
//    NSString *urlString =[NSString stringWithFormat:@"http://chepinshang.net"];
//    NSURL *url = [NSURL URLWithString:urlString];
//    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:url];
//    [webview loadRequest:request];
//    [self.view addSubview:webview];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(handleButtonAction)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Press me" forState:UIControlStateNormal];
    [button setBackgroundColor:[UIColor blueColor]];
    button.frame = CGRectMake(80.0, 210.0, 160.0, 40.0);
    [self.view addSubview:button];
}
    
- (void)handleButtonAction {
    FlutterViewController * flutterViewController = [[FlutterViewController alloc] init];
//    flutterViewController.view.backgroundColor = [UIColor redColor];
////    self.navigationController.navigationBar.hidden = NO;
    [flutterViewController setInitialRoute:@"myApp"];
//
//    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
////    [button addTarget:self
////               action:@selector(handleButtonAction)
////     forControlEvents:UIControlEventTouchUpInside];
//    [button setTitle:@"Press me" forState:UIControlStateNormal];
//    [button setBackgroundColor:[UIColor blueColor]];
//    button.frame = CGRectMake(80.0, 210.0, 160.0, 40.0);
//    [flutterViewController.view addSubview:button];
    
     [self.navigationController pushViewController:flutterViewController animated:YES];
//    [self presentViewController:flutterViewController animated:false completion:nil];
}
    

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
