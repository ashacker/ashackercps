//
//  VideoEditViewController.h
//  TCLVBIMDemo
//
//  Created by xiang zhang on 2017/4/10.
//  Copyright © 2017年 tencent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>


#import "Config.h"
#import "MOMNetWorking.h"
//#import "UIImageView+ASH.h"
#import "MOMProgressHUD.h"

#import "TXUGCPublish.h"

#import "TCHttpUtil.h"

@interface VideoEditViewController : UIViewController

@property (strong,nonatomic) NSString *videoPath;

@property (strong,nonatomic) AVAsset  *videoAsset;

@property (strong,nonatomic) NSArray  *imageList;

@property (assign, nonatomic) BOOL removeVideoAfterFinish;

@end
